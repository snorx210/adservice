﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ADServices.API.DTO {
    public class ADGroupUserDto {
        public string GroupName { get; set; }
        public string UserName { get; set; }
    }
}
