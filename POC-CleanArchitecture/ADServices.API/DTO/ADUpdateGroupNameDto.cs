﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ADServices.API.DTO {
    public class ADUpdateGroupNameDto {
        public string GroupName { get; set; }
        public string GroupNameTo { get; set; }
    }
}
