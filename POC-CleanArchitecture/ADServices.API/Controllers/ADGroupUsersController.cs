﻿using ADServices.API.DTO;
using ADServices.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ADServices.API.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ADGroupUsersController : ControllerBase {

        private readonly IADGroupUserService _groupUsersService;
        public ADGroupUsersController(IADGroupUserService adGroupUsersService) {

            this._groupUsersService = adGroupUsersService;
        }

        [HttpPatch("add")]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public IActionResult updateGroup([FromBody] ADGroupUserDto groupUser) {
            this._groupUsersService.AddToGroup(groupUser.GroupName, groupUser.UserName);

            return Ok();
        }

        [HttpDelete("remove")]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public IActionResult removeFromGroup([FromBody] ADGroupUserDto groupUser) {
            this._groupUsersService.RemoveFromGroup(groupUser.GroupName, groupUser.UserName);

            return Ok();
        }

        [HttpPut("changeName")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult ChangeGroupName([FromBody] ADUpdateGroupNameDto group) {
            this._groupUsersService.ChangeGroupName(group.GroupName, group.GroupNameTo);

            return Ok();
        }
    }
}
