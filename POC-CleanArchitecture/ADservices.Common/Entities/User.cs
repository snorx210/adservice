﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADservices.Common.Entities {
    public class User {
        public string Name { get; set; }
        public bool IsEnabled { get; set; }

    }
}
