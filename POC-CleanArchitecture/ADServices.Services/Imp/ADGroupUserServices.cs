﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADServices.Data.Repositories;
using ADServices.Services.Services;

namespace ADServices.Services.Imp {
    public class ADGroupUserServices : IADGroupUserService {

        private readonly IGroupUsersRepository _groupUsersRepository;

        public ADGroupUserServices(IGroupUsersRepository groupRepository) {
            this._groupUsersRepository = groupRepository;
        }
        public void AddToGroup(string groupName, string username) {
            this._groupUsersRepository.AddToGroup(groupName, username);
        }
        public void ChangeGroupName(string groupName, string groupNameTo) {
            this._groupUsersRepository.ChangeGroupName(groupName, groupNameTo);
        }
        public void RemoveFromGroup(string groupName, string username) {
            this._groupUsersRepository.RemoveFromGroup(groupName, username);
        }
    }
}
