﻿namespace ADServices.Services.Services {
    public interface IADGroupUserService {
        public void ChangeGroupName(string groupName, string groupNameTo);
        public void AddToGroup(string groupName, string username);
        public void RemoveFromGroup(string groupName, string username);
    }
}