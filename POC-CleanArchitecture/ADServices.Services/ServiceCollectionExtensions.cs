﻿using ADServices.Services.Imp;
using ADServices.Services.Services;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            //services.AddSingleton<IADUsersService, ADUsersService>();
            services.AddSingleton<IADGroupUserService, ADGroupUserServices>();

            return services;
        }
    }
}
