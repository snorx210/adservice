﻿using ADServices.Data;
using ADServices.Data.Impl;
using ADServices.Data.Repositories;

namespace Microsoft.Extensions.DependencyInjection {
    public static class ServiceCollectionExtensions {
        public static IServiceCollection AddRepositories(this IServiceCollection services) {
            //return services.AddSingleton<IUsersRepository, UsersRepository>();
            return services.AddSingleton<IGroupUsersRepository, GroupUsersService>();
        }
    }
}
