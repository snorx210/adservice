﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADServices.Data.Repositories;
using Microsoft.Extensions.Configuration;

namespace ADServices.Data.Impl {
    public class GroupUsersService : IGroupUsersRepository {

        private readonly string _ldapServerUrl;
        private readonly string _domainName;
        public GroupUsersService(IConfiguration config) {
            _ldapServerUrl = config.GetConnectionString("LDAP");
            _domainName = config.GetConnectionString("Domain");
        }
        private string GetDn(string name) {
            using (var rootEntry = new DirectoryEntry(this._ldapServerUrl + this._domainName)) {
                using (var directorySearcher = new DirectorySearcher(rootEntry, String.Format("(name={0})", name))) {
                    var searchResult = directorySearcher.FindOne();
                    if (searchResult != null) {
                        using (var userEntry = searchResult.GetDirectoryEntry()) {
                            return (string)userEntry.Properties["distinguishedName"].Value;
                        }
                    }
                }
            }
            return null;
        }
        public void AddToGroup(string groupName, string username) {
            string gDM = GetDn(groupName);
            string user = GetDn(username);
            DirectoryEntry dirEntry = new DirectoryEntry(this._ldapServerUrl + gDM);
            dirEntry.Properties["member"].Add(user);
            dirEntry.CommitChanges();
            dirEntry.Close();
            dirEntry.Dispose();
        }

        public void ChangeGroupName(string groupName, string groupNameTo) {
            string group = GetDn(groupName);
            DirectoryEntry domain = new DirectoryEntry(this._ldapServerUrl + group);
            domain.Properties["sAMAccountName"].Value = groupNameTo;
            domain.CommitChanges();
            domain.Rename($"cn={groupNameTo}"); 
            domain.CommitChanges();
            domain.Close();
            domain.Dispose();
        }

        public void RemoveFromGroup(string groupName, string username) {
            string gDM = GetDn(groupName);
            string user = GetDn(username);
            DirectoryEntry dirEntry = new DirectoryEntry(this._ldapServerUrl + gDM);
            dirEntry.Properties["member"].Remove(user);
            dirEntry.CommitChanges();
            dirEntry.Close();
            dirEntry.Dispose();
        }
    }
}
