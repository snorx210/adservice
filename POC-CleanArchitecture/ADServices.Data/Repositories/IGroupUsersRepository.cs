﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADServices.Data.Repositories {
    public interface IGroupUsersRepository {
        public void ChangeGroupName(string groupName, string groupNameTo);
        public void AddToGroup(string groupName, string username);
        public void RemoveFromGroup(string groupName, string username);

    }
}
